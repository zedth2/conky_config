#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
Author : Zachary Harvey






'''


from subprocess import check_output
from json import loads
from pprint import PrettyPrinter
from copy import deepcopy
from os.path import abspath
from sys import argv
from conkyconfig import *

SCRIPT = abspath(__file__)

def Mounts():
    pass

def AllDrives():
    dev = loads(check_output('lsblk -J'.split()).decode())
    re = ''
    chars = ['┃','┃','${'+WHITE+'}D${'+FOREGROUND+'}','${'+WHITE+'}R${'+FOREGROUND+'}','${'+WHITE+'}I${'+FOREGROUND+'}','${'+WHITE+'}V${'+FOREGROUND+'}','${'+WHITE+'}E${'+FOREGROUND+'}','${'+WHITE+'}S${'+FOREGROUND+'}']
    topCharCnt = 0
    for bd in dev['blockdevices']:
        topChar = '┃'
        if len(chars) > topCharCnt:
            topChar = chars[topCharCnt]
        topCharCnt += 1
        re += '${{{color}}}{topChar} {name}  {size: >15}iB {type}\n'.format(color=FOREGROUND,topChar=topChar, name=bd['name'], size=bd['size'], type=bd['type'])
        c = 0
        try:
            while len(bd['children']) > c:
                topChar = '┃'
                if len(chars) > topCharCnt:
                    topChar = chars[topCharCnt]
                topCharCnt += 1
                char = '├'
                if len(bd['children'])-1 == c:
                    char = '└'
                m = bd['children'][c]['mountpoint']
                if m == None:
                    m = bd['children'][c]['name']
                    re += '${{{color}}}{topChar} {char}{dsk: <4} {size: >15} {type: <8}\n'.format(color=FOREGROUND,topChar=topChar, char=char, dsk=bd['children'][c]['name'], size=bd['children'][c]['size']+'iB', type=bd['children'][c]['type'])
                else:
                    re += '${{{color}}}{topChar} {char}{dsk: <4} ${{fs_used {mnt}}} / ${{fs_size {mnt}}} {type: <8} {mnt}\n'.format(color=FOREGROUND,topChar=topChar, char=char, dsk=bd['children'][c]['name'], mnt=m, type=bd['children'][c]['type'])
                c += 1
        except KeyError as e:
            pass
    return re

def getAllDrives():
    merged = mergeDfLsblk()
    re = ''
    chars = ['┃','┃','${'+WHITE+'}D${'+FOREGROUND+'}','${'+WHITE+'}R${'+FOREGROUND+'}','${'+WHITE+'}I${'+FOREGROUND+'}','${'+WHITE+'}V${'+FOREGROUND+'}','${'+WHITE+'}E${'+FOREGROUND+'}','${'+WHITE+'}S${'+FOREGROUND+'}']
    sorted = list(merged.keys())
    sorted.sort()
    topCharCnt = 0
    for k in sorted:
        bd = merged[k]
        topChar = '┃'
        if len(chars) > topCharCnt:
            topChar = chars[topCharCnt]
        topCharCnt += 1
        re += '${{{color}}}{topChar} {name}  {size: >15}iB {type}\n'.format(color=FOREGROUND,topChar=topChar, name=bd['name'], size=bd['size'], type=bd['type'])
        c = 0
        try:
            while len(bd['children']) > c:
                topChar = '┃'
                if len(chars) > topCharCnt:
                    topChar = chars[topCharCnt]
                topCharCnt += 1
                char = '├'
                if len(bd['children']) - 1 == c:
                    char = '└'
                m = bd['children'][c]['mountpoint']
                if m == None:
                    m = bd['children'][c]['name']
                    re += '${{{color}}}{topChar} {char}{dsk: <4} {size: >15} {type: <8}\n'.format(color=FOREGROUND,topChar=topChar, char=char, dsk=bd['children'][c]['name'], size=bd['children'][c]['size']+'iB', type=bd['children'][c]['type'])
                else:
                    re += '${{{color}}}{topChar} {char}{dsk: <4} ${{fs_used {mnt}}} / ${{fs_size {mnt}}} {mnt}\n'.format(color=FOREGROUND,topChar=topChar, char=char, dsk=bd['children'][c]['name'], mnt=m, type=bd['children'][c]['type'])
                c += 1
        except KeyError as kerr:
            pass
    return re


def dictDf():
    reVal = {}
    n = len('/dev/')
    for l in check_output('df -hT'.split()).splitlines()[0:]:
        s = l.decode().split()
        if s[0].startswith('/dev/'):
            reVal[s[0][n:]] = dict(zip(['filesystem', 'type', 'size','used','avail','use%','mountpoint'],s))
    return reVal

def onlyTypes(types='disk'):
    '''
    Grab all of types from lsblk output.

    types: str, list, tuple
        Should either be a string of the type wanted
        or a list or tuple of the types wanted. Pass
        empty list or tuple to get all types
    '''
    if not isinstance(types, (tuple, list)):
        types = (types,)
    isEmpty = 0 == len(types)
    dev = loads(check_output('lsblk -J'.split()).decode())['blockdevices']
    devs ={}
    for d in dev:
        if d['type'] in types or isEmpty:
            devs.update({d['name']: d})
    return devs

def mergeDfLsblk(types=('disk',)):
    df = dictDf()
    lsblk = onlyTypes(types)
    cntBlk = -1

    for k in lsblk:
        d = lsblk[k]
        if not 'children' in d:
            cntBlk += 1
            continue
        cnt = 0
        while cnt < len(d['children']):
            c = d['children'][cnt]
            if c['name'] in df:
                lsblk[k]['children'][cnt]['type'] = df[c['name']]['type']
                lsblk[k]['children'][cnt]['used'] = df[c['name']]['used']
                lsblk[k]['children'][cnt]['avail'] = df[c['name']]['avail']
            cnt += 1
        cntBlk += 1
    return lsblk




def GetAllDriveInfo():
    dev = loads(check_output('lsblk -J'.split()).decode())
    #mnts = check_output('mount').decode().split()
    devs = []
    for l in check_output('df -hT'.split()).splitlines()[0:]:
        s = l.decode().split()
        if s[0].startswith('/dev/'):
            s[0] = s[0][5:]
            devs.append(dict(zip(['filesystem', 'type', 'size','used','avail','use%','mountpoint'],s)))
    devcpd = deepcopy(devs)
    disks = {}
    for b in dev['blockdevices']:
        if not len(devs):
            break
        for d in devs:
            if b['name'] == d['filesystem'][:-1]:
                try:
                    disks[b['name']].append(d)
                except KeyError:
                    disks.update({b['name']:[b,d]})
                devcpd.remove(d)
        devs = deepcopy(devcpd)

    #The Conky Build
    re = ''
    chars = ['┃','┃','${'+WHITE+'}D${'+FOREGROUND+'}','${'+WHITE+'}R${'+FOREGROUND+'}','${'+WHITE+'}I${'+FOREGROUND+'}','${'+WHITE+'}V${'+FOREGROUND+'}','${'+WHITE+'}E${'+FOREGROUND+'}','${'+WHITE+'}S${'+FOREGROUND+'}']
    topCharCnt = 0
    #PrettyPrinter().pprint(disks)
    names = list(disks.keys())
    names.sort()
    for n in names:
    #for d, v in disks.items(): #Do top level disk
        d, v = n, disks[n]
        topChar = '┃'
        if len(chars) > topCharCnt:
            topChar = chars[topCharCnt]
        topCharCnt += 1
        re += '${{{foreground}}}{topChar} {disk} {size: >10}\n'.format(foreground=FOREGROUND, topChar=topChar, disk=d, size=v[0]['size'])
        c = 1
        while len(v) > c:
        #for i in in v[1:]:
            topChar = '┃'
            if len(chars) > topCharCnt:
                topChar = chars[topCharCnt]
            topCharCnt += 1

            char = '├'
            mchar = '│'
            if len(v)-1 == c:
                char = '└'
                mchar = ' '
            percColor = GREEN
            if int(v[c]['use%'][:-1]) > 75:
                percColor = RED
            re += '${{{foreground}}}{topChar} ${{{percColor}}}{char}{dsk: >4} {used: >6} / {size: <6} {type: <4}\n'.format(foreground=FOREGROUND, topChar=topChar, char=char, percColor=percColor, dsk=v[c]['filesystem'], used=v[c]['used'], size=v[c]['size'], type=v[c]['type'])
            topChar = '┃'
            if len(chars) > topCharCnt:
                topChar = chars[topCharCnt]
            topCharCnt += 1
            re += '${{{foreground}}}{topChar}${{{percColor}}} {mchar}└ {mnt}${{{foreground}}}\n'.format(mchar=mchar,percColor=percColor, topChar=topChar, foreground=FOREGROUND, mnt=v[c]['mountpoint'])
            c += 1
    while topCharCnt < len(chars):
        re += chars[topCharCnt]+'\n'
        topCharCnt += 1
    return (re + '┃')

def ConkyText(drawTime):
    return '${{execpi {draw} python3 {script} -t}}\n'.format(draw=drawTime, script=SCRIPT)


def tester():
    from pprint import pprint
    pprint(mergeDfLsblk())


def Main():
    if len(argv) > 1 and argv[1] == '-t':
        print(getAllDrives())
        exit(0)
    txt = CONKY_TXT_BEGIN + ConkyText('30') + CONKY_TXT_END
    c = GetConfig('middle_left', maxwidth=512, gap_x=15, gap_y=0)
    #c['gap_x'] = '15'
    #c['gap_y'] = '0'
#    if 'LinDevPower' == HOSTNAME:
#        c['gap_x'] = '9'
#        c['gap_y'] = '1000'
#    else:
#        c['gap_x'] = '9'
#        c['gap_y'] = '580'
#    c['gap_x'] = '9'
#    c['gap_y'] = '934'
    c['update_interval'] = '30'
    WriteOutConfig(DIR+os.sep+HOSTNAME+'_drives', ConfigToLua(c), txt)
    RunConky(DIR+os.sep+HOSTNAME+'_drives')
    #print(AllDrives())
    #print(GetAllDriveInfo())

if __name__ == '__main__':
    #tester()
    Main()
