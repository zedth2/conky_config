#!/usr/bin/python3
'''
Author : Zachary Harvey






'''

from conkyconfig import *

MEM_PROC = 10

def Mem():
    re = "${"+FOREGROUND+"""}┃
┣━ram: """+\
"${if_match ${memperc} < 20}${"+GREEN+"}\
${else}\
${if_match ${memperc} < 40}${"+GREEN_YELLOW+"}\
${else}\
${if_match ${memperc} < 60}${"+YELLOW+"}\
${else}\
${if_match ${memperc} < 80}${"+RED+"}\
${else}${"+FOREGROUND+"}\
${endif}\
${endif}\
${endif}\
${endif}\
$memperc%$alignr$mem${"+FOREGROUND+"}/\
${if_match ${memperc} < 20}${"+GREEN+"}\
${else}\
${if_match ${memperc} < 40}${"+GREEN_YELLOW+"}\
${else}\
${if_match ${memperc} < 60}${"+YELLOW+"}\
${else}\
${if_match ${memperc} < 80}${"+RED+"}\
${else}${"+RED+"}\
${endif}\
${endif}\
${endif}\
${endif}"+"""$memmax
${"""+FOREGROUND+"""}┃ $membar
${"""+FOREGROUND+"}┣━memory usage${alignr}mem\n"
    c = 1
    while c <= MEM_PROC:
        re += "${"+FOREGROUND+"}┃ \
${if_match ${eval ${execp echo '${top_mem mem "+str(c)+"}' | xargs}} < 0.5}${"+GREEN+"}\
${else}\
${if_match ${eval ${execp echo '${top_mem mem "+str(c)+"}' | xargs}} < 2.1}${"+GREEN_YELLOW+"}\
${else}\
${if_match ${eval ${execp echo '${top_mem mem "+str(c)+"}' | xargs}} < 4.2}${"+YELLOW+"}\
${else}${if_match ${eval ${execp echo '${top_mem mem "+str(c)+"}' | xargs}} < 8.4}${"+RED+"}\
${else}${"+RED+"}\
${endif}\
${endif}\
${endif}\
${endif}\
${top_mem pid "+str(c)+"} : ${top_mem name "+str(c)+"}${alignr}${top_mem mem_res "+str(c)+"}\n"
        c += 1
    return re + "${"+FOREGROUND+"}┗━━━━${"+WHITE+"}MEMORY${"+FOREGROUND+"}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

def Main():
    txt = CONKY_TXT_BEGIN + Mem() + CONKY_TXT_END
    c = GetConfig('bottom_left')
    #c['maximum_width'] = '280'
    #if 'LinDevPower' == HOSTNAME:
        #c['gap_x'] = '1929'
        #c['gap_y'] = '-350'
    #else:
        #c['gap_x'] = '9'
        #c['gap_y'] = '10'
    c['gap_x'] = '9'
    c['gap_y'] = '10'

    WriteOutConfig(DIR+os.sep+HOSTNAME+"_mem", ConfigToLua(c), txt)
    #print(c,'\n\n',txt)
    RunConky(DIR+os.sep+HOSTNAME+"_mem")

if __name__ == '__main__':
    Main()
