#!/usr/bin/python3
'''
Author : Zachary Harvey






'''


from conkyconfig import *
from os.path import abspath
from sys import argv
import datetime
import calendar
CURYEAR = datetime.datetime.now().year
CURMONTH = datetime.datetime.now().month
CURDAY = datetime.datetime.now().day
SCRIPT = abspath(__file__)

def BuildMonth(month=CURMONTH, year=CURYEAR, highLightCurday=True, weekDays=True, highLightDays={}):
    '''
        highLightDays : {day : color}
    '''
    if month > 12:
        year += int(month / 12)
        month %= 12
    if highLightCurday and month == CURMONTH and year == CURYEAR:
        if CURDAY in highLightDays.keys():
            highLightDays[CURDAY] = str(hex(int(highLightDays[CURDAY], 16) | int(GREEN_HEX, 16)))[2:]
        else:
            highLightDays[CURDAY] = GREEN_HEX
    re =  '${{{foreground}}}┌'.format(foreground=FOREGROUND)
    monYear = calendar.month_name[month] + ' ' + str(year)
    leftOver = int((len(' Su Mo Tu We Th Fr Sa')-len(monYear))/2)
    re += ('─'*leftOver)+monYear+('─'*leftOver)+'\n'
    #───{month} {year}────────\n'.format(month=calendar.month_name[month], year=year, foreground=FOREGROUND)
    if weekDays:
        re += '│ Su Mo Tu We Th Fr Sa\n'
    wdays = 0
    for d in calendar.Calendar(6).itermonthdays(year,month):
        if 0 == wdays:
            re += '│'
        if 0 == d:
            re += '   '
        else:
            if d in highLightDays.keys():
                re += '${{color {}}} {:>2}${{{foreground}}}'.format(highLightDays[d], d, foreground=FOREGROUND)
            else:
                re += ' {:>2}'.format(d)
        wdays += 1
        if 7 == wdays:
            re += '\n'
            wdays = 0
    return re

def Main():
    txt = ''
    cnt = CURMONTH-1
    year = CURYEAR
    for m in range(CURMONTH, CURMONTH+3):
        if m > 12:
            if (m % 12) == 1:
                year += 1
            m %= 12
        txt += '\n' + BuildMonth(m, year, highLightDays={15:RED_HEX,calendar.monthrange(year,m)[1]:RED_HEX})
    if len(argv) > 1 and '-t' == argv[1]:
        print(txt)
        exit(0)
    txt = CONKY_TXT_BEGIN + '\n${execpi 21600 python3 '+SCRIPT+' -t}\n' + CONKY_TXT_END
    print('THE TEST', txt)
    cfg = GetConfig('middle_right')
    cfg['gap_x'] = '15'
#    if 'LinDevPower' == HOSTNAME:
#        cfg['gap_x'] = '3572'
#        cfg['gap_y'] = '1171'
#    else:
#        cfg['gap_x'] = '9'
#        cfg['gap_y'] = '500'
    cfg['update_interval'] = '21600'
    WriteOutConfig(DIR+os.sep+HOSTNAME+'_cal', ConfigToLua(cfg), txt)
    RunConky(DIR+os.sep+HOSTNAME+'_cal')


if __name__ == '__main__':
    Main()
