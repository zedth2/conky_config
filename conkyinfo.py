#!/usr/bin/python3
'''
Author : Zachary Harvey






'''

from sys import argv
from subprocess import check_output, CalledProcessError
from os.path import exists, abspath
from os import sep
from distutils.spawn import find_executable
from conkyconfig import *

SCRIPT = abspath(__file__)


def OSRelease():
    sysInfo = {}
    if exists('/etc/os-release'):
        for l in open('/etc/os-release').readlines():
            try:
                sysInfo[l.split('=')[0]] = l.split('=')[1].strip('"\n ')
            except IndexError:
                pass
    return sysInfo


def BuildConkyText():
    sysInfo = OSRelease()
    c = '${{{foreground}}}━━━━━{pretty}━━━━━\n'.format(
        pretty=sysInfo['PRETTY_NAME'], foreground=FOREGROUND)
    c += '${{{foreground}}}$alignc$kernel\n'.format(foreground=FOREGROUND)
    c += '${{{foreground}}}$alignc${{machine}}\n'.format(foreground=FOREGROUND)
    c += '${{{foreground}}}$alignc$nodename\n'.format(foreground=FOREGROUND)
    c += '${{{foreground}}}$alignc$uptime\n'.format(foreground=FOREGROUND)
    if find_executable('pacman') is not None:
        c += '${{{foreground}}}$alignc Installed: ${{execi 60 pacman -Qe | wc -l}}\n'.format(
            foreground=FOREGROUND)
        try:
            ups = len(check_output('pacman -Qu'.split()).splitlines())
            if ups:
                c += '${{{yellow}}}$alignc Updates: ${{execi 60 pacman -Qu | wc -l}}\n'.format(yellow=YELLOW)
        except CalledProcessError:
            try:  #This is stupid and I'm drunk
                ups = len(check_output('pacman -Qu'.split()).splitlines())
                if ups:
                    c += '${{{yellow}}}$alignc Updates: ${{execi 60 pacman -Qu | wc -l}}\n'.format(yellow=YELLOW)
            except CalledProcessError:
                pass
    return c


def ConkyUpdate():
    return CONKY_TXT_BEGIN + BuildConkyText() + CONKY_TXT_END


def Main():
    if len(argv) > 1 and argv[1] == '-t':
        print(BuildConkyText())
        exit(0)
    txt = CONKY_TXT_BEGIN + '${execpi 60 python3 ' + SCRIPT + ' -t}\n' + CONKY_TXT_END
    c = GetConfig('top_left')
    if 'LinDevPower' == HOSTNAME:
        #c['gap_x'] = '2284'
        c['gap_x'] = '1890'
        c['gap_y'] = '23'
    else:
        c['gap_x'] = '1000'
        c['gap_y'] = '23'

    c['update_interval'] = '60'
    WriteOutConfig(DIR + sep + HOSTNAME + '_info', ConfigToLua(c), txt)
    RunConky(DIR + sep + HOSTNAME + '_info')


if __name__ == '__main__':
    Main()
