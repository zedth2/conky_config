#!/usr/bin/python3
'''
Author : Zachary Harvey






'''

from conkyconfig import *

import os
from subprocess import getoutput, getstatusoutput
from sys import argv
from conky_notes import COLOR
import socket
from multiprocessing import cpu_count

CPU_CNT = cpu_count()

DIR = os.path.dirname(os.path.realpath(__file__))
MAXWIDTH = 300
MINWIDTH = 300
POSITION = 'top_middle'
CPU_PROC_CNT = 10
GRAD_COLOR_1 = "43b6b8"
GRAD_COLOR_2 = "FF00FF"
GRAPH_HEIGHT = 64
GRAPH_WIDTH = 128

def StackCpuGraph():
    cpugraph = "${{cpugraph cpu{cnt}"+ \
        " {height},{width} {bg} {fg}}}".format(height=GRAPH_HEIGHT, width=GRAPH_WIDTH, bg=GRAD_COLOR_1, fg=GRAD_COLOR_2) + "}"
    cpuperc = "{cnt} ${{goto {width}}} ${{cpu cpu{cnt}}}%"
    cpupercNext = "${{goto {width}}}"
    perRow = 4
    if 0 == (CPU_CNT % 4):
        perRow = 4
    elif 0 == (CPU_CNT % 3):
        perRow = 3
    else:
        perRow = 2

    graphs = []
    perc = []

    for i in range(CPU_CNT):
        if 0 == (i % perRow):
            #graphs.append(['┃ '])
            graphs.append([''])
            perc.append(['┃'])
        else:
            perc[-1].append(cpupercNext.format(width=60+((GRAPH_WIDTH)*(i%perRow))))
        graphs[-1].append(cpugraph.format(cnt=i)+" ")
        perc[-1].append(
            cpuperc.format(cnt=i, width=80+ ((12+GRAPH_WIDTH) * (i % perRow))))
        if 1 != ((i+1) % perRow):
            perc[-1][-1] += '${{goto {0}}}┃'.format((12+GRAPH_WIDTH)*(i%perRow))
    out = "${" + FOREGROUND + "}━━━━━${" + WHITE + "}CORES${" + FOREGROUND + "}" + ("━"* 41) +"\n"
    for i in range(len(graphs)):
        out += ''.join(graphs[i]) + '\n' + ''.join(perc[i]) + '\n'

    #out += '└' + ('━' * 51)
    #out += ('━' * 51)
    return out


def MakeCPUGraph():
    from multiprocessing import cpu_count
    cpu_cnt = cpu_count()
    Func = lambda c: "CPU" + str(c + 1) + " $alignr ${cpu cpu" + str(
        c + 1) + "}%\n${cpugraph cpu" + str(c + 1) + " 21," + str(
            MAXWIDTH) + " 000000 43b6b8}\n"
    ReStr = ''
    cnt = 0
    while cnt < cpu_cnt:
        ReStr += Func(cnt)
        cnt += 1
    return ReStr + '\n'


def MakeCPUGraphBoarder():
    PX_HEIGHT = 32
    from multiprocessing import cpu_count
    cpu_cnt = cpu_count()
    re = "${" + FOREGROUND + "}┏━━━━${" + WHITE + "}CORES${" + FOREGROUND + "}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n"
    func = lambda c: "${" + FOREGROUND + "}┃ CPU" + str(
        c) + " $alignr ${cpu cpu" + str(
            c) + "}%\n${" + FOREGROUND + "}┃ ${cpugraph cpu" + str(
                c) + " " + str(PX_HEIGHT) + "," + str(MINWIDTH - 25) + " 000000 FF00FF }\n"
    cnt = 0
    while cnt < cpu_cnt:
        re += func(cnt)
        cnt += 1
    return re


def MakeCPUAppList():
    c = 1
    re = ''
    while c <= CPU_PROC_CNT:
        #re += "${"+DARK_ORANGE+"}┃ ${if_match ${top cpu "+str(c)+"} < 1}${"+GREEN+"}${else}${if_match ${top cpu "+str(c)+"} < 2}${"+GREEN+"}${else}${if_match ${top cpu "+str(c)+"} < 4}${"+YELLOW+"}${else}${if_match ${top cpu "+str(c)+"} < 8}${color4}${else}${color4}${endif}${endif}${endif}${endif}${top name "+str(c)+"}$alignr${top cpu "+str(c)+"}\n"
        re += "${" + FOREGROUND + "}┃ \
${if_match ${top cpu " + str(c) + "} < 1}${" + GREEN + "}\
${else}${if_match ${top cpu " + str(c) + "} < 2}${" + GREEN_YELLOW + "}\
${else}\
${if_match ${top cpu " + str(c) + "} < 4}${" + YELLOW + "}\
${else}\
${if_match ${top cpu " + str(c) + "} < 8}${" + DARK_ORANGE + "}\
${else}${" + RED + "}\
${endif}\
${endif}\
${endif}\
${endif}\
${top pid " + str(c) + "} : ${top name " + str(c) + "}$alignr${top cpu " + str(
            c) + "}\n"
        c += 1
    return "${" + FOREGROUND + "}┏━━━━${" + WHITE + "}TOP PROCESSES${" + FOREGROUND + """}━━━━━━━━━━━━━━━━━━━━
┃
┣━cpu usage${alignr}%cpu\n""" + re


#${color1}┃ ${if_match ${top cpu 2} < 1}${color3}${else}${if_match ${top cpu 2} < 2}${color3}${else}${if_match ${top cpu 2} < 4}${color5}${else}${if_match ${top cpu 2} < 8}${color4}${else}${color4}${endif}${endif}${endif}${endif}${top name 2}$alignr${top cpu 2}
#${color1}┃ ${if_match ${top cpu 3} < 1}${color3}${else}${if_match ${top cpu 3} < 2}${color3}${else}${if_match ${top cpu 3} < 4}${color5}${else}${if_match ${top cpu 3} < 8}${color4}${else}${color4}${endif}${endif}${endif}${endif}${top name 3}$alignr${top cpu 3}
#${color1}┃ ${if_match ${top cpu 4} < 1}${color3}${else}${if_match ${top cpu 4} < 2}${color3}${else}${if_match ${top cpu 4} < 4}${color5}${else}${if_match ${top cpu 4} < 8}${color4}${else}${color4}${endif}${endif}${endif}${endif}${top name 4}$alignr${top cpu 4}
#${color1}┃ ${if_match ${top cpu 5} < 1}${color3}${else}${if_match ${top cpu 5} < 2}${color3}${else}${if_match ${top cpu 5} < 4}${color5}${else}${if_match ${top cpu 5} < 8}${color4}${else}${color4}${endif}${endif}${endif}${endif}${top name 5}$alignr${top cpu 5}
#${color1}┃"""


def Main(args):
    txt = CONKY_TXT_BEGIN + MakeCPUGraphBoarder() + MakeCPUAppList(
    ) + CONKY_TXT_END
    c = (GetConfig('top_left', maxwidth=MAXWIDTH + 20))
    #if 'LinDevPower' == HOSTNAME:
    #c['gap_x'] = '1929'
    #else:
    #c['gap_x'] = '9'
    c['gap_y'] = '23'
    c['gap_x'] = '9'
    WriteOutConfig(DIR + os.sep + HOSTNAME + '_proc', c, txt)
    RunConky(DIR + os.sep + HOSTNAME + '_proc')
    #print(c,'\n\n',txt)

def stackMain(*args):
    txt = CONKY_TXT_BEGIN + StackCpuGraph() + CONKY_TXT_END
    c = GetConfig('top_left', maxwidth=1000)

    c['gap_y'] = '15'
    c['gap_x'] = '450'
    WriteOutConfig(DIR + os.sep + HOSTNAME + '_proc', c, txt)
    RunConky(DIR + os.sep + HOSTNAME + '_proc')

def AppListMain(*args):
    txt = CONKY_TXT_BEGIN + MakeCPUAppList() + CONKY_TXT_END
    c = GetConfig('top_left', maxwidth=1000)

    c['gap_y'] = '15'
    c['gap_x'] = '15'
    WriteOutConfig(DIR + os.sep + HOSTNAME + '_applist', c, txt)
    RunConky(DIR + os.sep + HOSTNAME + '_applist')


if __name__ == '__main__':
    #Main('')
    stackMain()
    AppListMain()
