#!/usr/bin/python3
'''
Author : Zachary Harvey






'''

import os
import socket
from subprocess import check_output

from collections import OrderedDict

DIR=os.path.dirname(os.path.realpath(__file__))
HOSTNAME=socket.gethostname()
FILENAME=HOSTNAME+'_GENCONKYRC'
MAXWIDTH=300
MINWIDTH=240
POSITION='top_middle'
DESKTOP='LinDevForever'
CPU_PROC_CNT = 10
COLOR='8400FF'
CONKY_TXT_BEGIN = 'conky.text = [[\n'
CONKY_TXT_END = ']]\n'

BLACK = 'color0'
BLACK_HEX = "000000"
WHITE = 'color1'
WHITE_HEX = "FFFFFF"
GREEN = 'color2'
GREEN_HEX = "00FF00"
#DARK_GREEN = 'color3'
#DARK_GREEN_HEX =
YELLOW = 'color3'
YELLOW_HEX = 'FFFF00'
DARK_ORANGE = 'color4'
DARK_ORANGE_HEX = 'E53C00'
RED = 'color5'
RED_HEX = 'FF0000'
PURPLE = 'color6'
PURPLE_HEX = '8400FF'
BLUE = 'color7'
BLUE_HEX = '0000FF'
GREEN_YELLOW = 'color8'
GREEN_YELLOW_HEX = 'A3FF00'
LIGHT_BLUE = 'color9'
LIGHT_BLUE_HEX = '0097FF'
FOREGROUND = LIGHT_BLUE

FONT_SIZE = 8

ERROR = RED
WARNING = YELLOW
ALMOST_WARNING = DARK_ORANGE
GOOD = GREEN



def GetConfig(alignment='top_middle', maxwidth=512, minwidth=100, gap_x=10, gap_y=10):
    config = OrderedDict([
                ('alignment', "'"+alignment+"'"),
                ('gap_x', str(gap_x)),
                ('gap_y', str(gap_y)),
                ('background', 'true'),
                ('cpu_avg_samples', '2'),
                ('double_buffer', 'true'),
                ('draw_borders', 'false'),
                ('draw_graph_borders', 'true'),
                ('draw_outline', 'false'),
                ('draw_shades', 'false'),
                ('maximum_width', str(maxwidth)),
                ('minimum_height', '5'),
                ('minimum_width', str(minwidth)),
                #('minimum_height', str(MAXWIDTH) + ' 5'),
                ('no_buffers', 'true'),
                ('override_utf8_locale', 'true'),
                ('own_window', 'true'),
                ('own_window_argb_value', "180"),
                ('own_window_argb_visual', 'true'),
                ('own_window_hints', "'undecorated,below,sticky,skip_taskbar,skip_pager'"),
                ('own_window_type', "'override'"),
                ('total_run_times', '0'),
                ('update_interval', '4.0'),
                ('uppercase', 'false'),
                ('use_xft', 'true'),
                ('xftalpha', '0.5'),
                ('font', "'Source Code Pro:size={}'".format(FONT_SIZE)),
                ('default_color', "'#"+COLOR+"'"),
                (BLACK, "'{}'".format(BLACK_HEX)),
                (WHITE, "'{}'".format(WHITE_HEX)),
                (GREEN, "'{}'".format(GREEN_HEX)),
                (YELLOW, "'{}'".format(YELLOW_HEX)),
                (DARK_ORANGE, "'{}'".format(DARK_ORANGE_HEX)),
                (RED, "'{}'".format(RED_HEX)),
                (PURPLE, "'{}'".format(PURPLE_HEX)),
                (BLUE, "'{}'".format(BLUE_HEX)),
                (GREEN_YELLOW, "'{}'".format(GREEN_YELLOW_HEX)),
                (LIGHT_BLUE, "'{}'".format(LIGHT_BLUE_HEX)),
                ('xinerama_head', '0'),
                ])
    return config

def ConfigToLua(cfg):
    reConfig = 'conky.config = {\n'
    for k, v in cfg.items():
        reConfig += '\t'+k+' = '+v+',\n'
    return reConfig + '\n};\n'

def WriteOutConfig(filename, cfg, txt):
    c = cfg

    if isinstance(cfg, dict):
        c = ConfigToLua(cfg)
    open(filename, 'w', encoding='utf-8').write(c+'\n\n'+txt)
    return True

def GetScreenResolution():
    reVal = {}
    try:
        out = check_output('xrandr')
    except FileNotFoundError:
        return reVal
    for l in out.decode().splitlines():
        if l[0].isspace(): continue
        words = l.split()
        if words[1] == 'connected':
            if words[2] == 'primary':
                reVal.update({words[0]:words[3]})
            else:
                reVal.update({words[0]:words[2]})

    return reVal

def RunConky(filename):
    os.system("conky -c "+filename)
    return 0
