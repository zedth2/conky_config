#!/usr/bin/python3
'''
Author : Zachary Harvey






'''

from subprocess import getoutput, check_call, CalledProcessError, DEVNULL
from os import listdir
from conkyconfig import *

def Network():
    interLst = listdir('/sys/class/net')
    interLst.remove('lo')
    interLst.sort()
    #interLst = getoutput("ip link | \grep 'state UP' | cut -d : -f 2").splitlines()
    net = ''
    re = '${'+FOREGROUND+'}━━━━━━━━━━━━━━━━━━━━${'+WHITE+'}NETWORK${'+FOREGROUND+'}━┓\n'
    c = 0
    while len(interLst) > c:
        i = interLst[c].strip()
        re += ConfigInt(i) + ('$alignr┃\n' if len(interLst)-1 > c else '')
        c += 1
    return re

def ConfigInt(interface):
    wan = '''$alignr${{if_up {interface}}}${{{green}}}{interface}${{{foreground}}} :  WAN━┫
$alignr${{addr {interface}}} : IP─┤ ┃
$alignr {mac} : MAC┘ ┃
$alignr Inbound ${{downspeed {interface}}} kb/s┃
${{downspeedgraph {interface} 16,{minwidth} {purple} {green_hex} }}$alignr┃
$alignr Outbound ${{upspeed {interface}}} kb/s┃
${{upspeedgraph {interface} 16,{minwidth} {purple} {green_hex} }}$alignr┃
${{else}}\
$alignr${{{red}}}{interface}${{{foreground}}} :  WAN━┫
$alignr {mac} : MAC┘ ┃
${{endif}}'''.format(interface=interface, purple=PURPLE_HEX, green_hex=GREEN_HEX, green=GREEN, red=RED, foreground=FOREGROUND, minwidth=MINWIDTH, mac=open('/sys/class/net/'+interface+'/address').read().strip())

    return wan

def Main():
    txt = CONKY_TXT_BEGIN + (Network()) + CONKY_TXT_END
    c = GetConfig('top_right')
    #if 'LinDevPower' == HOSTNAME:
    #    c['gap_x'] = '-890'
    #else:
    #    c['gap_x'] = '9'
    c['gap_x'] = '15'
    c['gap_y'] = '15'
    c = WriteOutConfig(DIR+os.sep+HOSTNAME+'_network', ConfigToLua(c), txt)

    RunConky(DIR+os.sep+HOSTNAME+'_network')

if __name__ == '__main__':
    Main()
