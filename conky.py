#!/usr/bin/python3
import os
from subprocess import getoutput, getstatusoutput
from sys import argv
from conky_notes import COLOR
import socket

DIR = os.path.dirname(os.path.realpath(__file__))
HOSTNAME = socket.gethostname()
FILENAME = HOSTNAME + '_GENCONKYRC'
MAXWIDTH = 300
MINWIDTH = 250
POSITION = 'top_middle'
DESKTOP = 'LinDevForever'
CPU_PROC_CNT = 10




def GetConfig(alignment='top_middle',
              maxwidth=320,
              minwidth=320,
              gap_x=128,
              gap_y=48):
    config = {
        'alignment': "'" + alignment + "'",
        'background': 'true',
        'cpu_avg_samples': '2',
        'default_color': "'#" + COLOR + "'",
        'double_buffer': 'true',
        'draw_borders': 'false',
        'draw_graph_borders': 'true',
        'draw_outline': 'false',
        'draw_shades': 'false',
        'gap_x': str(gap_x),
        'gap_y': str(gap_y),
        'maximum_width': str(maxwidth),
        'minimum_height': '5',
        'minimum_width': str(minwidth),
        #'minimum_size': str(MAXWIDTH) + ' 5',
        'no_buffers': 'true',
        #'override_utf8_locale': 'false',
        'own_window': 'true',
        'own_window_argb_value': "180",
        'own_window_argb_visual': 'true',
        'own_window_hints':
        "'undecorated,below,sticky,skip_taskbar,skip_pager'",
        'own_window_type': "'override'",
        'total_run_times': '0',
        'update_interval': '4.0',
        'uppercase': 'false',
        'use_xft': 'true',
        'xftalpha': '0.5',
        'font': "'Source Code Pro:size=06'",
        'color0': "'FFFFFF'",  #--Black
        'color1': "'E53C00'",  #--Orange
        'color2': "'8400FF'",  #--Purple
        'color3': "'00FF00'",  #--Green
        'color4': "'FF0000'",  #--Red
        'color5': "'FFFF00'",  #--Yellow
        'color6': "'FF7700'",  #--Dark Orange
        'color7': "'0000FF'",  #--Blue
    }
    return config


def ConfigToLua(cfg):
    reConfig = 'conky.config = {\n'
    for k, v in cfg.items():
        reConfig += '\t' + k + ' = ' + v + ',\n'

    return reConfig + '\n};\n'


def GetInterface():
    interLst = getoutput(
        "ip link | \grep 'state UP' | cut -d : -f 2").splitlines()
    net = ''
    for inter in interLst:
        inter = inter.strip()
        net += inter + ' $alignr ${addr ' + inter + '}\nInbound $alignr ${downspeed ' + inter + '} kb/s\n${downspeedgraph ' + inter + '}\nOutbound $alignr ${upspeed ' + inter + '} kb/s\n${upspeedgraph ' + inter + '}\n'
    return net + '\n'


def MakeCPUGraph():
    from multiprocessing import cpu_count
    cpu_cnt = cpu_count()
    Func = lambda c: "CPU" + str(c + 1) + " $alignr ${cpu cpu" + str(
        c + 1) + "}%\n${cpugraph cpu" + str(c + 1) + " 21," + str(
            MAXWIDTH) + " 000000 43b6b8}\n"
    ReStr = ''
    cnt = 0
    while cnt < cpu_cnt:
        ReStr += Func(cnt)
        cnt += 1
    return ReStr + '\n'


def MakeCPUGraphBoarder():
    from multiprocessing import cpu_count
    cpu_cnt = cpu_count()
    re = "${color1}┏━━━━${color0}Cores${color1}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n"
    func = lambda c: "${color1}┃ CPU" + str(c) + " $alignr ${cpu cpu" + str(
        c) + "}%\n${color1}┃ ${cpugraph cpu" + str(c) + " 16," + str(
            MINWIDTH) + " 000000 FF00FF }\n"
    cnt = 0
    while cnt < cpu_cnt:
        re += func(cnt)
        cnt += 1
    return re


def MakeCPUAppList():
    c = 1
    re = ''
    while c <= CPU_PROC_CNT:
        re += "${color1}┃ ${if_match ${top cpu " + str(
            c
        ) + "} < 1}${color3}${else}${if_match ${top cpu " + str(
            c
        ) + "} < 2}${color3}${else}${if_match ${top cpu " + str(
            c
        ) + "} < 4}${color5}${else}${if_match ${top cpu " + str(
            c
        ) + "} < 8}${color4}${else}${color4}${endif}${endif}${endif}${endif}${top name " + str(
            c) + "}$alignr${top cpu " + str(c) + "}\n"
        c += 1
    return """${color1}┣━━━━${color0}TOP PROCESSES${color1}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
┃
┣━cpu usage${alignr}%cpu\n""" + re


#${color1}┃ ${if_match ${top cpu 2} < 1}${color3}${else}${if_match ${top cpu 2} < 2}${color3}${else}${if_match ${top cpu 2} < 4}${color5}${else}${if_match ${top cpu 2} < 8}${color4}${else}${color4}${endif}${endif}${endif}${endif}${top name 2}$alignr${top cpu 2}
#${color1}┃ ${if_match ${top cpu 3} < 1}${color3}${else}${if_match ${top cpu 3} < 2}${color3}${else}${if_match ${top cpu 3} < 4}${color5}${else}${if_match ${top cpu 3} < 8}${color4}${else}${color4}${endif}${endif}${endif}${endif}${top name 3}$alignr${top cpu 3}
#${color1}┃ ${if_match ${top cpu 4} < 1}${color3}${else}${if_match ${top cpu 4} < 2}${color3}${else}${if_match ${top cpu 4} < 4}${color5}${else}${if_match ${top cpu 4} < 8}${color4}${else}${color4}${endif}${endif}${endif}${endif}${top name 4}$alignr${top cpu 4}
#${color1}┃ ${if_match ${top cpu 5} < 1}${color3}${else}${if_match ${top cpu 5} < 2}${color3}${else}${if_match ${top cpu 5} < 4}${color5}${else}${if_match ${top cpu 5} < 8}${color4}${else}${color4}${endif}${endif}${endif}${endif}${top name 5}$alignr${top cpu 5}
#${color1}┃"""


def ProcMon(NUM=10):
    ReStr = 'NAME $alignr PID    CPU\n'
    cnt = 1
    #Func = lambda c : "
    Func = lambda c: "${top name " + str(c) + "} $alignr ${top pid " + str(
        c) + "} ${top cpu " + str(c) + "}\n"
    while cnt <= NUM:
        ReStr += Func(cnt)
        cnt += 1
    return ReStr


def Mounts():
    ReStr = ''
    out = getoutput("mount | \grep '^/dev'")
    out = out.splitlines()
    for i in out:
        mntpnt = i.split()[2]
        dev = i.split()[0]
        ReStr += dev.rsplit('/', 1)[-1] + ' - ' + mntpnt.rsplit(
            '/', 1
        )[-1] + " $alignc ${fs_used " + mntpnt + "} / ${fs_size " + mntpnt + "} $alignr ${fs_free_perc " + mntpnt + "}%\n${fs_bar " + mntpnt + "}\n"
    return ReStr


def Mem():
    ReStr = "MEM $alignc $mem / $memmax $alignr $memperc%\n$membar\n"
    if getoutput(" cat /proc/swaps | tail -n +2"):
        ReStr += 'SWAP $alignr ${swapperc}%\n${swapbar 5,' + str(
            MAXWIDTH) + '}\n'
    return ReStr + '\n'


def Battery():
    x = '\n'
    from glob import glob
    from os.path import basename
    for i in glob('/sys/class/power_supply/BAT*'):
        x += 'Battery: ${battery_percent ' + basename(
            i) + '}% ${alignr}${battery_bar 8,70 ' + basename(i) + '}\n'
    return x + '\n'


def Dropbox():
    re = getstatusoutput('which dropbox-cli')
    if 0 == re[0]:
        return '''
Dropbox
${hr}
${exec dropbox-cli status}
'''
    else:
        return ''


"""
def Update():
    return '''
Updates
${hr}
${exec paup}
'''
"""

#def GetText():
#return 'conky.text = [[\n' + '''$alignc $sysname $kernel on $machine
#Uptime $alignr $uptime
#Load $alignr $loadavg
#Hostname $alignr $nodename\n'''+GetInterface()+MakeCPUGraph()+MakeCPUAppList()+Mem()+Mounts()+Battery()+Dropbox() + '\n]]'


def GetText():
    return 'conky.text = [[\n' + '''$alignc $sysname $kernel on $machine
Uptime $alignr $uptime
Load $alignr $loadavg
Hostname $alignr $nodename\n''' + GetInterface() + MakeCPUGraphBoarder(
    ) + "${color1}┃\n" + MakeCPUAppList() + Mem() + Mounts() + Battery(
    ) + Dropbox() + '\n]]'


def Journal(FILENAME='JOURNALCTL_CONKY',
            MAXWIDTH=700,
            POSITION='top_middle',
            gap_x=528,
            gap_y=376):
    return '''conky.text = [[
background yes
use_xft yes
xftfont HandelGotD:size=9
xftalpha 0.5
update_interval 60.0
total_run_times 0
own_window yes
own_window_type normal
#own_window_transparent yes
own_window_hints undecorated,below,sticky,skip_taskbar,skip_pager
default_color ''' + COLOR + '''
double_buffer yes
minimum_size ''' + str(MAXWIDTH) + ''' 5

maximum_width ''' + str(MAXWIDTH) + '''
draw_shades no
draw_outline no
draw_borders no
draw_graph_borders yes
own_window_argb_visual yes
own_window_argb_value 220 255 255 255
alignment ''' + 'top_left' + '''
gap_x ''' + str(gap_x) + '''
gap_y ''' + str(gap_y) + '''
no_buffers yes
uppercase no
cpu_avg_samples 2
override_utf8_locale no

TEXT
Journalctl
${hr}
${exec journalctl -lr -q -b }}
'''


def GetGdkScreenInfo():
    from gi.repository import Gdk
    mons = []
    m = Gdk.Display().get_default()
    for i in range(m.get_n_monitors()):
        rect = m.get_monitor(i)
        mons.insert(i, tuple(rect.width, rect.height))
    return tuple(mons)


def GetScreenSize():
    from tkinter import Tk
    root = Tk()
    size = (root.winfo_screenwidth(), root.winfo_screenheight())
    root.destroy()
    return tuple([size])



def main(args):
    #if os.path.exists(DIR+os.sep+FILENAME):
    #os.system("conky -c "+DIR+os.sep+FILENAME)
    #return 0

    size = GetScreenSize()
    isFourK = is4K()
    Jor = ''
    if not isFourK:
        config = ConfigToLua(
            GetConfig(alignment="top_left", gap_x=15, gap_y=30))
        #Jor = Journal()
    else:
        config = ConfigToLua(
            GetConfig(alignment='top_left', gap_x=10, gap_y=50))
        #Jor = Journal(gap_x=1000)
    if '-p' in args:
        print('Printing to', DIR + os.sep + FILENAME)
        print(config + GetText())
    else:
        open(DIR + os.sep + FILENAME, 'w').write(config + GetText())
        os.system("conky -c " + DIR + os.sep + FILENAME)

    if len(args) > 1 and '-j' == args[1]:
        if '-p' in args:
            print('Printing to', DIR + os.sep + 'JOURNALCTL_CONKY')
            print(Jor)
        else:
            open(DIR + os.sep + 'JOURNALCTL_CONKY', 'w').write(Jor)
            os.system("conky -c " + DIR + os.sep + 'JOURNALCTL_CONKY')
    return 0


if __name__ == '__main__':
    exit(main(argv))
