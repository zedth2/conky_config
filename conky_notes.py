#!/usr/bin/python3
import os
import sys
from subprocess import getoutput
import platform
DIR=os.path.dirname(os.path.realpath(__file__))
print(DIR, os.path.realpath(__file__))
FILENAME=platform.node()+'_NOTECONKYRC'
COLOR='8400FF'
from subprocess import getoutput

def GetHeader(FILENAME='NOTECONKYRC', MAXWIDTH=500, POSITION='top_right', gap_x=45, gap_y=10):
    header ='''background yes
use_xft yes
xftfont HandelGotD:size=9
xftalpha 0.5
update_interval 200.0
total_run_times 0
own_window yes
own_window_type normal
#own_window_transparent yes
own_window_hints undecorated,below,sticky,skip_taskbar,skip_pager
default_color '''+COLOR+'''
double_buffer yes
minimum_size '''+str(MAXWIDTH)+' '+str(750)+'''

maximum_width '''+str(MAXWIDTH)+'''
draw_shades no
draw_outline no
draw_borders no
draw_graph_borders yes
own_window_argb_visual yes
own_window_argb_value 220 255 255 255
alignment '''+POSITION+'''
gap_x '''+str(gap_x)+'''
#gap_y '''+str(gap_y)+'''
no_buffers yes
uppercase no
cpu_avg_samples 2
override_utf8_locale no

TEXT

'''
    return header

def Notes():
    header = GetHeader(POSITION='middle_right')
    open(DIR+os.sep+FILENAME, 'w').write(header+'${exec sticky -c}')
    os.system("conky -c "+DIR+os.sep+FILENAME)
    return 0

def Clock(FILENAME=platform.node()+'_CLOCKCONKYRC'):
    if os.path.exists(FILENAME):
        os.system("conky -c "+DIR+os.sep+FILENAME)
        return 0

    if platform.node() == 'LinDevPower':
        POS='top_middle'
        GAPX = -120
    elif platform.node() == 'LinDevForeverWrk':
        POS='top_middle'
        GAPX = 256
    else:
        POS = 'top_right'
        GAPX = 150
    header = '''# Use Xft?
use_xft yes
xftfont HandelGotD:size=9
xftalpha 0.5
update_interval 15.0
own_window yes
own_window_hints undecorated,above,sticky,skip_taskbar,skip_pager
default_color '''+COLOR+'''
double_buffer yes
draw_outline yes
alignment '''+POS+'''
gap_x '''+str(GAPX)+'''
gap_y 0

TEXT
${font Mono:pixelsize=15}${alignc}${time %I:%M}${alignc}  ${time %A} | ${time %d} ${time  %B} ${time %Y}${font}
'''
    open(DIR+os.sep+FILENAME, 'w').write(header)
    os.system("conky -c "+DIR+os.sep+FILENAME)
    return 0

if __name__ == '__main__':
    if sys.argv[1] == '-n':
        exit(Notes())
    if sys.argv[1] == '-c':
        exit(Clock())
