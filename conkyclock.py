#!/usr/bin/python3
'''
Author : Zachary Harvey






'''

from conkyconfig import *

def ClockBuild():
    return '${{{foreground}}}${{time %I.%M}}'.format(foreground=FOREGROUND)

def Main():
    cfg = GetConfig('top_right')
    del cfg['minimum_width']
    del cfg['minimum_height']
    del cfg['maximum_width']
    cfg['gap_x'] = '512'
    cfg['gap_y'] = '0'
    cfg['own_window_hints'] = "'undecorated,above,sticky,skip_taskbar,skip_pager'"
    cfg['own_window_argb_value'] = '200'
    txt = CONKY_TXT_BEGIN + ClockBuild() + CONKY_TXT_END
    c = WriteOutConfig(DIR+os.sep+HOSTNAME+'_clock', ConfigToLua(cfg), txt)
    RunConky(DIR+os.sep+HOSTNAME+'_clock')

if __name__ == '__main__':
    Main()
